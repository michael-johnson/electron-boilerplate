# Electron Boilerplate

A boilerplate for using Electron-Webpack, Typescript, and React to create Electron apps.

## Usage

1. Clone this repo
2. `rm -rf .git`
3. Update the Readme
4. Update the package.json (`name`, `description`, `repository`, `bugs` and `homepage`).
5. Commit